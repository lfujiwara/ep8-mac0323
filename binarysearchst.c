/*
 * MAC0323 Estruturas de Dados e Algoritmo II
 *
 * Tabela de simbolos implementada atraves de vetores ordenados
 * redeminsionaveis
 *
 *     https://algs4.cs.princeton.edu/31elementary/BinarySearchST.java.html
 *
 * As chaves e valores desta implementação são mais ou menos
 * genéricos
 */

/* interface para o uso da funcao deste módulo */
#include "binarysearchst.h"

#include <stdlib.h>  /* free() */
#include <string.h>  /* memcpy() */
#include "util.h"    /* emalloc(), ecalloc() */

#undef DEBUG
#ifdef DEBUG
#include <stdio.h>   /* printf(): para debug */
#endif

/*
 * CONSTANTES
 */

/*------------------------------------------------------------*/
/*
 * Funções administrativas
 */
void resize(BinarySearchST st);
void shiftRight(BinarySearchST st, int k);
void shiftLeft(BinarySearchST st, int k);
/*----------------------------------------------------------*/
/*
 * Estrutura Básica da Tabela de Símbolos:
 *
 * implementação com vetores ordenados
 */

struct cell {
    /* Estrutura para guardar conteúdo + tamanho */
    void* data;
    size_t size;
};

struct binarySearchST {
    /* Chaves => Valores */
    struct cell* keys;
    struct cell* values;
    /* Tamanho */
    int n;
    /* Capacidade */
    int cap;
    /* Função de comparação */
    int (*cmp) (const void *key1, const void *key2);
    /* Iterador para keys */
    int keysIterator;
};

/*
 *  initST(COMPAR)
 *
 *  RECEBE uma função COMPAR() para comparar chaves.
 *  RETORNA (referência/ponteiro para) uma tabela de símbolos vazia.
 *
 *  É esperado que COMPAR() tenha o seguinte comportamento:
 *
 *      COMPAR(key1, key2) retorna um inteiro < 0 se key1 <  key2
 *      COMPAR(key1, key2) retorna 0              se key1 == key2
 *      COMPAR(key1, key2) retorna um inteiro > 0 se key1 >  key2
 *
 *  TODAS OS OPERAÇÕES da ST criada utilizam a COMPAR() para comparar
 *  chaves.
 *
 */
BinarySearchST
initST(int (*compar)(const void *key1, const void *key2))
{
    BinarySearchST bst = malloc(sizeof(struct binarySearchST));
    bst->keys = malloc(2*sizeof(struct cell));
    bst->values = malloc(2*sizeof(struct cell));
    bst->n = 0;
    bst->cap = 2;
    bst->cmp = compar;
    bst->keysIterator = -1;
    return bst;
}

/*-----------------------------------------------------------*/
/*
 *  freeST(ST)
 *
 *  RECEBE uma BinarySearchST  ST e devolve ao sistema toda a memoria
 *  utilizada por ST.
 *
 */
void
freeST(BinarySearchST st)
{
    /* Libera toda a memória ocupada pela ST */
    int i;
    for (i = 0; i < st->n; i++) {
        /* Apaga chaves e valores */
        free(st->keys[i].data);
        free(st->values[i].data);
    }
    /* Apaga vetores */
    free(st->keys);
    free(st->values);
    /* Apaga o ponteiro para a estrutura */
    free(st);
}

/*------------------------------------------------------------*/
/*
 * OPERAÇÕES USUAIS: put(), get(), contains(), delete(),
 * size() e isEmpty().
 */

/*-----------------------------------------------------------*/
/*
 *  put(ST, KEY, NKEY, VAL, NVAL)
 *
 *  RECEBE a tabela de símbolos ST e um par KEY-VAL e procura a KEY na ST.
 *
 *     - se VAL é NULL, a entrada da chave KEY é removida da ST
 *
 *     - se KEY nao e' encontrada: o par KEY-VAL é inserido na ST
 *
 *     - se KEY e' encontra: o valor correspondente é atualizado
 *
 *  NKEY é o número de bytes de KEY e NVAL é o número de bytes de NVAL.
 *
 *  Para criar uma copia/clone de KEY é usado o seu número de bytes NKEY.
 *  Para criar uma copia/clode de VAL é usado o seu número de bytes NVAL.
 *
 */
void
put(BinarySearchST st, const void *key, size_t nKey, const void *val, size_t nVal)
{
    int i;
    if (val == NULL) { delete(st, key); return; } /* Se val == null, deleta */

    i = rank(st, key); /* Busca binária */
    if (isEmpty(st)) i = 0;
    /* Se bb é bem-sucedida, libera valor antigo p/ inserir novo */
    if ((i >= 0 && i < st->n) && st->cmp(st->keys[i].data, key) == 0) {
        free(st->values[i].data); /* Libera valor antigo */
    }
    else { /* Se não, shift para direita para inserir novo valor e insere a chave */
        resize(st);
        shiftRight(st, i);
        st->keys[i].data = malloc(nKey);
        memcpy(st->keys[i].data, key, nKey);
        st->keys[i].size = nKey;
        st->n++;
    }

    st->values[i].data = malloc(nVal); /* Aloca novo espaço */
    memcpy(st->values[i].data, val, nVal); /* Copia conteúdo */
    st->values[i].size = nVal; /* Atualiza tamanho */
}

/*-----------------------------------------------------------*/
/*
 *  get(ST, KEY)
 *
 *  RECEBE uma tabela de símbolos ST e uma chave KEY.
 *
 *     - se KEY está em ST, RETORNA NULL;
 *
 *     - se KEY não está em ST, RETORNA uma cópia/clone do valor
 *       associado a KEY.
 *
 */
void *
get(BinarySearchST st, const void *key)
{
    int i = rank(st, key);
    if (i < 0 || i >= st->n) return NULL;
    if (st->cmp(st->keys[i].data, key) == 0) {
        void* copy = malloc(st->values[i].size);
        memcpy(copy, st->values[i].data, st->values[i].size);
        return copy;
    }
    return NULL;
}

/*-----------------------------------------------------------*/
/*
 *  CONTAINS(ST, KEY)
 *
 *  RECEBE uma tabela de símbolos ST e uma chave KEY.
 *
 *  RETORNA TRUE se KEY está na ST e FALSE em caso contrário.
 *
 */
Bool
contains(BinarySearchST st, const void *key)
{
    int i = rank(st, key);
    if (i < 0 || i >= st->n) return FALSE;
    return (st->cmp(st->keys[i].data, key) == 0) ? TRUE:FALSE;
}

/*-----------------------------------------------------------*/
/*
 *  DELETE(ST, KEY)
 *
 *  RECEBE uma tabela de símbolos ST e uma chave KEY.
 *void
 *  Se KEY está em ST, remove a entrada correspondente a KEY.
 *  Se KEY não está em ST, faz nada.
 *
 */
void
delete(BinarySearchST st, const void *key)
{
    int i = rank(st, key);
    if (i < 0 || i >= st->n) return; /* Se estiver out of bounds, retorna */
    if (st->cmp(st->keys[i].data, key) != 0) return; /* Se não está lá, retorna */

    free(st->keys[i].data); /* Libera chave */
    free(st->values[i].data); /* Libera valor */

    shiftLeft(st, i); /* Shift para esquerda*/
    st->n = st->n - 1; /* Decrementa n */
    resize(st); /* Redimensiona o vetor */
}

/*-----------------------------------------------------------*/
/*
 *  SIZE(ST)
 *
 *  RECEBE uma tabela de símbolos ST.
 *
 *  RETORNA o número de itens (= pares chave-valor) na ST.
 *
 */
int
size(BinarySearchST st)
{
    return st->n;
}

/*-----------------------------------------------------------*/
/*
 *  ISEMPTY(ST, KEY)
 *
 *  RECEBE uma tabela de símbolos ST.
 *
 *  RETORNA TRUE se ST está vazia e FALSE em caso contrário.
 *
 */
Bool
isEmpty(BinarySearchST st)
{
    return (st->n == 0);
}


/*------------------------------------------------------------*/
/*
 * OPERAÇÕES PARA TABELAS DE SÍMBOLOS ORDENADAS:
 * min(), max(), rank(), select(), deleteMin() e deleteMax().
 */

/*-----------------------------------------------------------*/
/*
 *  MIN(ST)
 *
 *  RECEBE uma tabela de símbolos ST e RETORNA uma cópia/clone
 *  da menor chave na tabela.
 *
 *  Se ST está vazia RETORNA NULL.
 *
 */
void *
min(BinarySearchST st)
{
    void* copy;
    if (isEmpty(st)) return NULL;
    copy = malloc(st->keys[0].size);
    memcpy(copy, st->keys[0].data, st->keys[0].size);
    return copy;
}

/*-----------------------------------------------------------*/
/*
 *  MAX(ST)
 *
 *  RECEBE uma tabela de símbolos ST e RETORNA uma cópia/clone
 *  da maior chave na tabela.
 *
 *  Se ST está vazia RETORNA NULL.
 *
 */
void *
max(BinarySearchST st)
{
    void* copy;
    if (isEmpty(st)) return NULL;
    copy = malloc(st->keys[st->n-1].size);
    memcpy(copy, st->keys[st->n - 1].data, st->keys[st->n - 1].size);
    return copy;
}

/*-----------------------------------------------------------*/
/*
 *  RANK(ST, KEY)
 *
 *  RECEBE uma tabela de símbolos ST e uma chave KEY.
 *  RETORNA o número de chaves em ST menores que KEY.
 *
 *  Se ST está vazia RETORNA EXIT_FAILURE. (Especificado pelo Coelho no Paca)
 *
 */
int
rank(BinarySearchST st, const void *key)
{
    int lo, hi, mid, cmpr;
    if (isEmpty(st)) return 0;
    lo = 0, hi = st->n - 1;
    while (lo <= hi) {
        mid = (lo+hi)/2;
        cmpr = st->cmp(key, st->keys[mid].data);
        if (cmpr == 0) return mid;
        if (cmpr > 0) lo = mid+1;
        else hi = mid-1;
    }
    return lo;
}

/*-----------------------------------------------------------*/
/*
 *  SELECT(ST, K)
 *
 *  RECEBE uma tabela de símbolos ST e um inteiro K >= 0.
 *  RETORNA a (K+1)-ésima menor chave da tabela ST.
 *
 *  Se ST não tem K+1 elementos RETORNA NULL.
 *
 */
void *
select(BinarySearchST st, int k)
{
    void* copy;
    if (k >= st->n || k < 0) return NULL;
    copy = malloc(st->keys[k].size);
    memcpy(copy, st->keys[k].data, st->keys[k].size);
    return copy;
}

/*-----------------------------------------------------------*/
/*
 *  deleteMIN(ST)
 *
 *  RECEBE uma tabela de símbolos ST e remove a entrada correspondente
 *  à menor chave.
 *
 *  Se ST está vazia, faz nada.
 *
 */
void
deleteMin(BinarySearchST st)
{
    void* minv = min(st);
    delete(st, minv);
    free(minv);
}

/*-----------------------------------------------------------*/
/*
 *  deleteMAX(ST)
 *
 *  RECEBE uma tabela de símbolos ST e remove a entrada correspondente
 *  à maior chave.
 *
 *  Se ST está vazia, faz nada.
 *
 */
void
deleteMax(BinarySearchST st)
{
    void* maxv = max(st);
    delete(st, maxv);
    free(maxv);
}

/*-----------------------------------------------------------*/
/*
 *  KEYS(ST, INIT)
 *
 *  RECEBE uma tabela de símbolos ST e um Bool INIT.
 *
 *  Se INIT é TRUE, KEYS() RETORNA uma cópia/clone da menor chave na ST.
 *  Se INIT é FALSE, KEYS() RETORNA a chave sucessora da última chave retornada.
 *  Se ST está vazia ou não há sucessora da última chave retornada, KEYS() RETORNA NULL.
 *
 *  Se entre duas chamadas de KEYS() a ST é alterada, o comportamento é
 *  indefinido.
 *
 */
void *
keys(BinarySearchST st, Bool init)
{
    if (init) st->keysIterator = 0; /* Resetando iterador */
    /* Se não há próximo... */
    if (st->keysIterator < 0 || st->keysIterator >= st->n) return NULL;
    /* Incrementando iterador */
    st->keysIterator++;
    /* Retorna valor do iterator - 1 */
    return select(st, st->keysIterator - 1);
}

/*-----------------------------------------------------------*/
/*
  Visit each entry on the ST.

  The VISIT function is called, in-order, with each pair key-value in the ST.
  If the VISIT function returns zero, then the iteration stops.

  visitST returns zero if the iteration was stopped by the visit function,
  nonzero otherwise.
*/
int
visitST(BinarySearchST st, int (*visit)(const void *key, const void *val))
{
    int i;
    for (i = 0; i < st->n; i++)
        if (!visit(st->keys[i].data, st->values[i].data))
            return 0;
    return 1;
}


/*------------------------------------------------------------*/
/*
 * Funções administrativas
 */
void resize(BinarySearchST st) {
    struct cell *k, *v;

    /* Verificação (assim podemos chamar resize sem verificar) */
    if (st->n < st->cap && st->n > (st->cap)/4) return;
    /* Limiar de capacidade */

    /* Capacidade nova */
    st->cap = (st->n >= st->cap) ? 2*(st->cap):(st->cap)/2;

    /* Alocando nova memória */
    k = realloc(st->keys, (st->cap)*sizeof(struct cell));
    v = realloc(st->values, (st->cap)*sizeof(struct cell));

    /* Apontando para nova memória e atualizando capacidade */
    st->keys = k;
    st->values = v;
}

void shiftRight(BinarySearchST st, int k) {
    /*
        Realiza shift para direita nos arrays a partir do índice k,
        liberando espaço para inserção
    */
    int i;
    if (isEmpty(st)) return;
    resize(st); /* Resize p/ aplicar operações c/ segurança */
    for (i = st->n-1; i >= k; i--) {
        st->keys[i+1].data = st->keys[i].data;
        st->keys[i+1].size = st->keys[i].size;
        st->values[i+1].data = st->values[i].data;
        st->values[i+1].size = st->values[i].size;
    }
}

void shiftLeft(BinarySearchST st, int k) {
    /*
        Realiza shift para esquerda nos arrays a partir do índice k,
        justapondo os valores após deleção
    */
   int i;
   for (i = k; i < st->n - 1; i++) {
        st->keys[i].data = st->keys[i+1].data;
        st->keys[i].size = st->keys[i+1].size;
        st->values[i].data = st->values[i+1].data;
        st->values[i].size = st->values[i+1].size;
   }
}