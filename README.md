# EP8 - MAC0323 - BinarySearchST

Exercício Programa 8 - Algoritmos e Estruturas de Dados II (IME-USP)

## Implementação - Estrutura

- 2 vetores `struct cell* v` 
  - `struct cell`
    - `void* ` representa o valor
    - `size` representa o tamanho em bytes
  - Vetor `keys[]` ordenado
    - Utiliza função de comparação fornecida na criação
  - Vetor `values[]`  'alinhado' com o vetor `key`
- Função de comparação
- `int n` para indicar tamanho da tabela
- `int cap` para indicar a capacidade

# API

```c
/*-----------------------------------------------------------*/
/* 
 *  Construtor e "destrutor"
 */
/* 
 *  INIT(COMPAR)
 *
 *  cria uma ST vazia 
 *  compar() é ponteiro para uma função que retorna um inteiro tal que
 *
 *      compar(key1, key2) retorna um inteiro < 0 se key1 <  key2
 *      compar(key1, key2) retorna um 0           se key1 == key2
 *      compar(key1, key2) retorna um inteiro > 0 se key1 >  key2
 */
BinarySearchST
initST(int (*compar)(const void *key1, const void *key2));

/* libera todo o espaço usado pela ST */
void  
freeST(BinarySearchST st);

/*------------------------------------------------------------*/
/*
 * Permite as operações usuais: put(), get(), contains(), delete(),
 * size() e isEmpty().
 */

/* coloca um par KEY-VAL na ST */
void  
put(BinarySearchST st, const void *key, size_t nKey, const void *val, size_t nVal);

/* retorna o valor associado a KEY */
void * 
get(BinarySearchST st, const void *key);

/* retorna TRUE se KEY esta na ST e FALSE em caso contrário */
Bool
contains(BinarySearchST st, const void *key);

/* remove da ST a chave KEY */
void
delete(BinarySearchST st, const void *key);

/* retorna o numero de itens na ST */
int
size(BinarySearchST st);

/* retorna TRUE se ST esta vazia e FALSE em caso contrário */
Bool
isEmpty(BinarySearchST st);

/*------------------------------------------------------------*/
/*
 * Fornece algumas operações para tabelas de símbolos ordenadas: 
 * min(), max(), rank(), select(), deleteMin() e deleteMax().
 */

/* menor chave na ST */
void *
min(BinarySearchST st);

/* maior chave na ST */
void *
max(BinarySearchST st);

/* numero de chaves menores que key */
int
rank(BinarySearchST st, const void *key);

/* remove menor chave */
void
deleteMin(BinarySearchST st);

/* remove menor chave */
void
deleteMax(BinarySearchST st);

/* a k-ésima chave da ST */
void *
select(BinarySearchST st, int k);

/*------------------------------------------------------------*/
/*
 * Também fornece a operação keys() para iterar todas as chaves.
 */

/* all of the keys, as an Iterable */
void *
keys(BinarySearchST st, Bool init);

/*
  Visit each entry on the ST.

  The VISIT function is called, in-order, with each pair key-value in the ST.
  If the VISIT function returns zero, then the iteration stops.

  visitST returns zero if the iteration was stopped by the visit function,
  nonzero otherwise.
*/
int
visitST(BinarySearchST st, int (*visit)(const void *key, const void *val));
```

